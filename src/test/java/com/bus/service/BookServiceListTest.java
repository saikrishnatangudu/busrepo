package com.bus.service;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.bus.exception.UserNotfoundException;
import com.bus.model.User;
import com.bus.repository.UserRepo;


@RunWith(MockitoJUnitRunner.Silent.class)
public class BookServiceListTest {

	@InjectMocks
	BookListImpl userServiceImpl;

	@Mock
	UserRepo userRepository;

	@Test
	public void testFindByIdForNegative() {
		User user = new User(-15, "jansi", "Jansi", "jansi@gmail.com", "9566497854", "Female");
		Mockito.when(userRepository.findById(-15)).thenReturn(Optional.of(user));
		User user1 = userServiceImpl.getUserById(-15);
		Assert.assertNotNull(user1);
		Assert.assertEquals("jansi@gmail.com", user1.getEmail());
	}

	@Test
	public void testFindByIdForPositive() {
		User user = new User(20, "jansi", "Jansi", "jenni@gmail.com", "9566497859", "Female");
		Mockito.when(userRepository.findById(20)).thenReturn(Optional.of(user));
		User user1 = userServiceImpl.getUserById(20);
		Assert.assertNotNull(user1);
		Assert.assertEquals("jenni@gmail.com", user1.getEmail());
	}

	@Test(expected = UserNotfoundException.class)
	public void testFindByIdForException() throws UserNotfoundException {
		User user = new User(20, "jansi", "Jansi", "jenni@gmail.com", "9566497859", "Female");
		Mockito.when(userRepository.findById(20)).thenReturn(Optional.of(user));
		User user1 = userServiceImpl.getUserById(14);
		Assert.assertNotNull(user1);
		Assert.assertEquals("jenni@gmail.com", user1.getEmail());
	}

}
