package com.bus.service;


import com.bus.dto.UserLoginDto;
import com.bus.exception.UserNotfoundException;
import com.bus.model.User;


public interface LoginService {

	User login(UserLoginDto userLoginDto) throws UserNotfoundException ;

}
