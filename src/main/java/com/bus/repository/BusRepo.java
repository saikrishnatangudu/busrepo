package com.bus.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bus.model.Bus;

public interface BusRepo extends JpaRepository<Bus, Integer>{
	List<Bus> findBusBySourceAndDestinationAndJourneyDate(String source, String destination, Date date);

}
